<?php
spl_autoload_register( 'autoload' );

function autoload( $class ) {
	$classParts = explode( '\\', $class );// ['Application','Models','Product']
	$className  = implode( '/', $classParts ); // Application/Models/Product
	$className  .= '.php';// Application/Models/Product.php
	if ( file_exists( $className ) ) {
		include_once $className;
	}
}