<?php
namespace App;

class Cache {
	private static $instance=null;
	private function __construct() {

	}

	public static function getInstance()
	{
		if(is_null(self::$instance)){
			self::$instance = new Cache;
		}
		return self::$instance;
	}


}

$cache = Cache::getInstance();