<?php
require "functions.php";
$products = getProducts();
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
	      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>لیست محصولات</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
	<style>
		body{
			padding: 50px 0;
			direction: rtl;
		}
	</style>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

</head>
<body>
<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<div class="panel panel-default">
				<div class="panel-heading">محصولات</div>
				<div class="panel-body">
					<div class="row">
						<?php while ($product = $products->fetch(PDO::FETCH_ASSOC)): ?>
							<div class="col-xs-12 col-sm-4">
								<h4><?php echo $product['product_title']; ?></h4>
								<p><?php echo number_format($product['product_price']); ?></p>
								<a href="add_to_cart.php?product_id=<?php echo $product['product_id']; ?>" class="btn btn-success">اضافه به سبد خرید</a>
							</div>
						<?php endwhile; ?>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>
</body>
</html>

