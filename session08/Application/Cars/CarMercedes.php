<?php
namespace Application\Cars;

use Application\Cars\Contract\VehicleInterface;

class CarMercedes implements VehicleInterface {

	public function setColor( string $color ) {
		print "CarMercedes With Color :{$color}";
	}

	public function addAMGTuning()
	{
		// do additional tuning here
	}
}