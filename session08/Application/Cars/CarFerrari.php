<?php

namespace Application\Cars;


use Application\Cars\Contract\VehicleInterface;

class CarFerrari implements VehicleInterface {

	public function setColor( string $color ) {
		print "CarFerrari With Color :{$color}";
	}
}