<?php
namespace Application\Cars\Contract;

interface VehicleInterface {

	public function setColor(string $color);

}