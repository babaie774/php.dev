<?php
namespace Application\Factory;


use Application\Cars\Bicycle;
use Application\Cars\CarFerrari;
use Application\Cars\Contract\VehicleInterface;
use Application\Factory\Contract\FactoryMethod;

class ItalianFactory extends FactoryMethod {

	protected function createVehicle( string $type ): VehicleInterface {
		switch ($type) {
			case parent::CHEAP:
				return new Bicycle;
			case parent::FAST:
				return new CarFerrari;
			default:
				throw new \InvalidArgumentException("$type is not a valid vehicle");
		}
	}
}