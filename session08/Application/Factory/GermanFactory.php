<?php
namespace Application\Factory;


use Application\Cars\Bicycle;
use Application\Cars\CarMercedes;
use Application\Cars\Contract\VehicleInterface;
use Application\Factory\Contract\FactoryMethod;

class GermanFactory extends FactoryMethod {

	protected function createVehicle( string $type ): VehicleInterface  {
		switch ($type) {
			case parent::CHEAP:
				return new Bicycle;
			case parent::FAST:
				return new CarMercedes;
			default:
				throw new \InvalidArgumentException("$type is not a valid vehicle");
		}
	}
}