<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 11/15/2017
 * Time: 6:51 PM
 */

namespace Application\Notification\Gateways\SMS;


use Application\Notification\Gateways\SMS\Contract\SMSInterface;

class ParandSms implements SMSInterface {

	public function send( array $to, $message ) {
		$sender = new Parand();
		$sender->sendSMS($to,$message);
	}
}