<?php
require "functions.php";
$code = null;
$errors = [];
$hasError = false;
if ( ! empty( $_POST['fullName'] ) && ! empty( $_POST['mobile'] ) && ! empty( $_POST['content'] ) ) {
	$fullName = $_POST['fullName'];
	$mobile   = $_POST['mobile'];
	$content  = $_POST['content'];
	if(empty($fullName)){
		$hasError = true;
		$errors[]='نام شما نمی تواند خالی باشد.';
	}
	if(!ctype_digit($mobile))
	{
		$hasError = true;
		$errors[]='شماره موبایل باید به صورت عدد باشد.';
	}
	if(strlen($mobile) <> 11){
		$hasError = true;
		$errors[]= 'شماره موبایل حتما باید دارای 11 رقم باشد';
	}
	if(!$hasError){
		$code = insertData($fullName,$mobile,$content);
	}
}
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
	      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>نتیجه ثبت درخواست</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
	<style>
		body{
			padding: 50px 0;
			direction: rtl;
		}
	</style>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

</head>
<body>
<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<div class="panel panel-default">
				<div class="panel-heading">نتیجه ثبت درخواست</div>
				<div class="panel-body">
					<?php if(count($errors) > 0): ?>
						<div class="alert alert-danger">
						<?php foreach ($errors as $error): ?>
							<p><?php echo $error; ?></p>
						<?php endforeach; ?>
						</div>
					<?php endif; ?>

					<?php if(!empty($code)): ?>
						<div class="alert alert-success">
							<p>درخواست شما با موفقیت ثبت گردید.</p>
							<p>کد رهگیری شما:<b><?php echo $code; ?></b></p>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>

	</div>
</div>
</body>
</html>

