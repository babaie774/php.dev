<!doctype html>
<html lang="fa">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>سامانه ثبت شکایات مردمی</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
	<style>
		body{
			padding: 50px 0;
			direction: rtl;
		}
	</style>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>
<body>
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div class="panel panel-default">
						<div class="panel-heading">ثبت شکایات</div>
						<div class="panel-body">
							<form class="form-horizontal" action="handle.php" method="post">
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-2 control-label">نام کامل :</label>
									<div class="col-sm-10">
										<input type="text" name="fullName" class="form-control" id="inputEmail3" placeholder="مثل پدرام بهرامی" required>
									</div>
								</div>
								<div class="form-group">
									<label for="inputPassword3" class="col-sm-2 control-label">شماره تماس :</label>
									<div class="col-sm-10">
										<input type="text" name="mobile" class="form-control" id="inputPassword3" placeholder="مثل 091200000000">
									</div>
								</div>
								<div class="form-group">
									<label for="inputPassword3" class="col-sm-2 control-label">متن درخواست :</label>
									<div class="col-sm-10">
										<textarea class="form-control" name="content" rows="3"></textarea>									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-offset-2 col-sm-10">
										<button type="submit" name="submit" class="btn btn-default">ثبت درخواست</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>

			</div>
		</div>
</body>
</html>