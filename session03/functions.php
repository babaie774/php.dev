<?php
require "db.php";
function insertData( $fullName, $mobile, $content ) {
	global $db;
	$created_at = date( "Y-m-d H:i:s" );
	$code       = md5( $fullName . $mobile . $content );
	$code       = substr( $code, 0, 10 );
	$code       = strtoupper( $code );
	$stmt       = $db->prepare( "INSERT INTO complaints(`full_name`,`mobile`,`content`,`created_at`,`code`)
									   VALUES('{$fullName}','{$mobile}','$content','{$created_at}','{$code}')" );
	$stmt->execute();

	return $code;
}

function getData() {
	global $db;
	$sql = "SELECT c.*,u.first_name,u.last_name 
			FROM complaints c
			LEFT JOIN users u ON c.user_id=u.user_id";
	if(isset($_GET['keyword']) && !empty($_GET['keyword'])){
		$keyword = $_GET['keyword'];
		$sql.=" WHERE c.full_name LIKE '%{$keyword}%' OR u.first_name LIKE '%{$keyword}%'";
	}
	if ( isset( $_GET['order'] ) && ! empty( $_GET['order'] ) ) {
		$order = $_GET['order'];
		$sql   .= " ORDER BY created_at {$order}";//ORDER BY RAND()
	}
	$stmt = $db->prepare( $sql );
	$stmt->execute();
	return $stmt;

}

function isValidCodeInUrl(){
	return isset($_GET['code']);
}

function setReply( $id, $replyContent ) {

	global $db;
	$stmt = $db->prepare( "UPDATE complaints SET reply='{$replyContent}' WHERE id={$id} LIMIT 1" );

	return $stmt->execute();

}

function findReply( $code ) {
	global $db, $table;
	$code = strtoupper( $code );
	$stmt = $db->prepare( "SELECT reply FROM {$table} WHERE code='{$code}' LIMIT 1" );
	$stmt->execute();

	return $stmt;
}