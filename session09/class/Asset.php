<?php

class Asset {
	const BASEURL = 'http://php.dev/session09/';
	public static function __callStatic( $name, $arguments ) {
		$url = self::BASEURL . $name . '/' . $arguments[0] . '.';
		if ( isset( $arguments[1] ) ) {
			return $url . $arguments[1];
		}

		return $url . $name;
	}

//	public function css( $name ) {
//		return 'http://php.dev/session09/css/'.$name.'.css';
//	}
//
//	public function js( $name ) {
//		return 'http://php.dev/session09/js/'.$name.'.js';
//	}

//	public $number;
//	public function __construct() {
////		$this->number = 10;
//	}

//	public function __get( $name ) {
//		print $name;
//	}
//
//	public function __invoke() {
//
//	}
//
//	public function __call( $name,$arguments  ) {
//
//	}


}

//$a = new Asset();
//$a();
//$a->number;
//$a->sample;
//$a->doAction(1,2,3);
//
//Asset::some