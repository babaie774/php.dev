<?php
include "class-entity.php";

class User extends Entity {
	const ADMIN = 1;
	const USER = 2;
	const EDITOR = 3;
	const AUTHOR = 4;

	public function __construct() {
		parent::__construct();
		$this->table      = 'users';
		$this->primaryKey = 'user_id';
	}

	public function getAllAdmins(  ) {
		$type = User::ADMIN;
		$this->stmt = $this->connection->prepare( "SELECT * FROM {$this->table} WHERE type={$type}" );
	}

	public function save() {
		// TODO: Implement sample() method.
	}

}

$u = new User();
$sample_user = $u->find(2,array('first_name'));
print_r($sample_user);
